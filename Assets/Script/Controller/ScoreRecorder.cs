using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreRecorder : MonoBehaviour
{
    public int score;
    // Start is called before the first frame update
    void Start()
    {
        score = 0;
    }

    public void Reset(){
        score = 0;
    }

    // Update is called once per frame
    public void RecordScore(int score)
    {
       this.score += score;
    }

    public int ReturnScore(){
        return this.score;
    }
}