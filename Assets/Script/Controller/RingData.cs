using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingData : MonoBehaviour {
    public int score;

    public void Start(){
        if(this.gameObject.name=="1"){
            score = 5;
        }
        else if(this.gameObject.name=="2"){
            score = 4;
        }
        else if(this.gameObject.name=="3"){
            score = 3;
        }
        else if(this.gameObject.name=="4"){
            score = 2;
        }
        else{
            score = 1;
        }
    }
}
