using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingController : MonoBehaviour
{
    public ScoreRecorder scoreRecorder;
    public ISceneController sceneController;
    
    // Start is called before the first frame update
    void Start()
    {
        sceneController = SSDirector.GetInstance().CurrentScenceController as FirstController;
        scoreRecorder = Singleton<ScoreRecorder>.Instance;
        this.gameObject.AddComponent<RingData>();
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider arrowCollider){
        Debug.Log("碰撞！");
        Transform arrow = arrowCollider.gameObject.transform.parent;
        Debug.Log(arrow.gameObject.name);
        if(arrow == null){
            Debug.Log("null");
            return;
        }
        scoreRecorder.RecordScore(this.gameObject.GetComponent<RingData>().score);
        Debug.Log("Score:"+this.gameObject.GetComponent<RingData>().score);

        
        arrow.GetComponent<Rigidbody>().isKinematic = true;
        arrow.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        arrowCollider.gameObject.SetActive(false);
        arrow.tag = "onTarget";
    }
}
