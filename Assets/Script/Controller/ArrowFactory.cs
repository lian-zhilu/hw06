using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowFactory : MonoBehaviour
{
    private List<GameObject> used;
    private List<GameObject> free;
    public GameObject arrow;
    FirstController firstController;
    // Start is called before the first frame update
    void Start()
    {
        firstController = (FirstController)SSDirector.GetInstance().CurrentScenceController;
        used = new List<GameObject>();
        free = new List<GameObject>();
        arrow = null;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameObject GetArrow(){
        if(free.Count > 0){
            arrow = free[0];
            free.Remove(free[0]);
            if(arrow.tag == "onTarget")//箭在靶子上
            {
                arrow.GetComponent<Rigidbody>().isKinematic = false;
                arrow.tag = "arrow";
                arrow.transform.GetChild(0).gameObject.SetActive(true);
            }
            arrow.SetActive(true);
        }
        else{
            arrow = Instantiate(Resources.Load<GameObject>("Prefabs/arrow1"));
            arrow.GetComponent<Rigidbody>().isKinematic = false;
            // arrow.AddComponent<Rigidbody>();
        }
        // firstController = (FirstController)SSDirector.GetInstance().CurrentScenceController;
        // Transform pos = firstController.bow.transform.GetChild(0);
        // arrow.transform.position = pos.transform.position;
        arrow.transform.parent = firstController.bow.transform;   // 把arrow附加在bow上，跟随着bow移动
        arrow.transform.position = firstController.bow.transform.position;
        used.Add(arrow);

        return arrow;
    }

    public void FreeArrow(GameObject arrow){
        foreach(GameObject aa in used){
            Debug.Log("befreed");
            if(aa.gameObject.GetInstanceID() == arrow.GetInstanceID()){
                arrow.SetActive(false);
                used.Remove(aa);
                aa.tag = "arrow";
                break;
            }
        }
    }

    public void Clear(){
        foreach(GameObject dd in used){
            dd.transform.position = new Vector3(0, 0, 100);
            Destroy(dd);
        }
        foreach(GameObject dd in free){
            dd.transform.position = new Vector3(0, 0, 100);
            Destroy(dd);
        }
        used.Clear();
        free.Clear();
    }
}
