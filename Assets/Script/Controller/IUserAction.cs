using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUserAction
{
    public void Restart();
    public void ShootArrow();
    public void MoveBow(float x, float y);
}