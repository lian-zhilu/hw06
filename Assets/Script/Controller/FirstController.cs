using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstController : MonoBehaviour, ISceneController, IUserAction
{
    public CCShootActionManager actionManager;
    public ArrowFactory arrowFactory;
    public ScoreRecorder scoreRecorder;
    public UserGUI userGUI;

    public GameObject bow;
    public GameObject arrow;
    public GameObject target;
    private List<GameObject> arrows = new List<GameObject>();
    private int shoot_count = 0;
    private int status;
    Vector3 wind;
    float power;
    int wind_power;

    int round;
    int count;
    // Start is called before the first frame update
    void Start()
    {
        SSDirector director = SSDirector.GetInstance();
        // SSDirector.GetInstance().CurrentScenceController = this;
        gameObject.AddComponent<ArrowFactory>();
        gameObject.AddComponent<CCShootActionManager>();
        gameObject.AddComponent<ScoreRecorder>();
        userGUI = gameObject.AddComponent<UserGUI>();
        director.CurrentScenceController = this;
        
        arrowFactory = Singleton<ArrowFactory>.Instance;
        actionManager = Singleton<CCShootActionManager>.Instance;
        scoreRecorder = Singleton<ScoreRecorder>.Instance;
        // scoreRecorder = Singleton<ScoreRecorder>.Instance;
        // director.CurrentScenceController = this;
        // actionManager = this.gameObject.AddComponent<CCShootActionManager>() as CCShootActionManager;

        LoadResources();

        round = 1;
        count = 0;
        status = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(status == 1){
            if(Input.GetKeyDown(KeyCode.Space)){
                shoot_count+=1;
                if(shoot_count > 1){
                    shoot_count = 0;
                    return;
                }
                Debug.Log("InGUI"+shoot_count);
                ShootArrow();
            }
            float x = Input.GetAxis("Vertical");
            float y = Input.GetAxis("Horizontal");
            MoveBow(x,y);
            for(int i = 0; i < arrows.Count; i++){
                if(arrows[i].transform.position.z > 30){
                    arrowFactory.FreeArrow(arrows[i]);
                    arrows.RemoveAt(i);
                }
            }
            userGUI.score = scoreRecorder.ReturnScore();
            userGUI.count = count;
            userGUI.status = status;
            userGUI.wind = wind;
            userGUI.power = wind_power;
            if(count >= 5){
                status = 2;
                userGUI.score = scoreRecorder.ReturnScore();
                userGUI.count = count;
                userGUI.status = status;
            }
        }
    }

    public void LoadResources(){
        scoreRecorder.Reset();
        bow = Instantiate(Resources.Load("Prefabs/bow",typeof(GameObject))) as GameObject;
        target = Instantiate(Resources.Load("Prefabs/target",typeof(GameObject))) as GameObject;
    }

    public void ShootArrow(){
        if(userGUI.status == 1){
            float x = Random.RandomRange(0, 10);
            float y = Random.RandomRange(0, 10);
            power = Random.RandomRange(0, 1.2f);
            power *= 10;
            wind_power = (int)power;
            wind = new Vector3(x, y, 0);
            shoot_count+=1;
            Debug.Log("isCalled"+shoot_count);
            arrow = arrowFactory.GetArrow();
            arrow.GetComponent<Rigidbody>().isKinematic = false;
            arrows.Add(arrow);
            actionManager.Shoot(arrow, wind*power);
            count += 1;
        }
    }

    public void MoveBow(float x, float y){
        if(status == 1){
            bow.transform.position = new Vector3(bow.transform.position.x,bow.transform.position.y,-5);
            if(bow.transform.position.x > 5)
            {
                bow.transform.position = new Vector3(5,bow.transform.position.y,bow.transform.position.z);
                return;
            }
            if(bow.transform.position.x < -5)
            {
                bow.transform.position = new Vector3(-5,bow.transform.position.y,bow.transform.position.z);
                return;
            }
            if(bow.transform.position.y > 5)
            {
                bow.transform.position = new Vector3(bow.transform.position.x,5,bow.transform.position.z);
                return;
            }
            if(bow.transform.position.y < -5)
            {
                bow.transform.position = new Vector3(bow.transform.position.x,-5,bow.transform.position.z);
                return;
            }
            bow.transform.Translate(new Vector3(0,-y,x)*Time.deltaTime);
        }
    }

    public void Restart(){
        count = 0;
        scoreRecorder.Reset();
        status = 1;
        for(int i = 0; i < arrows.Count; i++){
            arrowFactory.FreeArrow(arrows[i]);
        }
        arrowFactory.Clear();
        arrows.Clear();
    }
}