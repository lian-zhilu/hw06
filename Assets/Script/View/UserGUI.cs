using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserGUI : MonoBehaviour
{
    GUIStyle topicStyle;
    GUIStyle dataStyle;
    public int count;
    public int status;  // 0: 未开始; 1: 进行中; 2: 结束
    public int score;
    private IUserAction userAction;
    private int shoot_count = 0;
    public Vector3 wind;
    public float power;
    // private bool isStart;
    // Start is called before the first frame update
    void Start()
    {
        status = 0;
        score = 0;
        userAction = SSDirector.GetInstance().CurrentScenceController as IUserAction;
        SetStyle();
    }

    void SetStyle(){
        topicStyle = new GUIStyle();
        topicStyle.normal.textColor = Color.black;
        topicStyle.fontSize = 60;

        dataStyle = new GUIStyle();
        dataStyle.normal.textColor = Color.black;
        dataStyle.fontSize = 30;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnGUI(){
        if(status == 0){
            if(GUI.Button(new Rect(10, 10, 100, 40), "Start Game")){
                status = 1;
                userAction.Restart();
            }
        }
        else if(status == 1){
            GUI.Label(new Rect(20, 20, 100, 50), "score: "+ score, dataStyle);
            GUI.Label(new Rect(20, 80, 100, 50), "round: "+ count, dataStyle);
            GUI.Label(new Rect(20, 140, 100, 50), "wind: ("+wind.x+", "+wind.y+", "+wind.z+")", dataStyle);
            GUI.Label(new Rect(20, 200, 100, 50), "power: "+ power, dataStyle);
            // if(Input.GetKeyDown(KeyCode.Space)){
            //     shoot_count+=1;
            //     if(shoot_count > 1){
            //         shoot_count = 0;
            //         return;
            //     }
            //     Debug.Log("InGUI"+shoot_count);
            //     userAction.ShootArrow();
            // }
            // float x = Input.GetAxis("Vertical");
            // float y = Input.GetAxis("Horizontal");
            // userAction.MoveBow(x, y);
        }
        else if(status == 2){
            GUI.Label(new Rect(120, 100, 50, 40), "Game Over", dataStyle);
            GUI.Label(new Rect(120, 200, 50, 40), "total score: " + score, dataStyle);
            if (GUI.Button(new Rect(120, 250, 100, 40), "Restart"))
            {
                status = 1;
                userAction.Restart();
            }
        }
    }
}
