using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCShootAction : SSAction
{
    Vector3 force;
    Vector3 wind;

    private CCShootAction(){}

    // Start is called before the first frame update
    public override void Start()
    {
        gameObject.transform.parent = null;
        gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;

        // force = new Vector3(0, 0, 30);
        // Impulse：向此刚体添加瞬时力冲击，考虑其质量。
        gameObject.GetComponent<Rigidbody>().AddForce(force, ForceMode.Impulse);
        gameObject.GetComponent<Rigidbody>().AddForce(wind, ForceMode.Impulse);
        gameObject.GetComponent<Rigidbody>().isKinematic = false;
    }

    // Update is called once per frame
    public override void Update()
    {

    }

    public override void FixedUpdate(){
        // 箭飞到场外或者中靶
        if(this.transform.position.z > 35 || this.transform.tag == "onTarget"){
            Debug.Log("回调");
            this.destroy = true;
            this.enable = false;
            this.callback.SSActionEvent(this, SSActionEventType.Completed, 0, null, gameObject);
        }
    }

    public static CCShootAction GetSSAction(Vector3 wind){
        CCShootAction shoot = CreateInstance<CCShootAction>();
        shoot.force = new Vector3(0, 0, 40);
        shoot.wind = wind;
        return shoot;
    }
}