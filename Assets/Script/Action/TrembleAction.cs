using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrembleAction : SSAction
{
    float tremble_radius = 0.05f;
    float tremble_time = 0.5f; 
    GameObject arrow;

    Vector3 arrow_pos;

    private TrembleAction(){}
    public static TrembleAction GetSSAction()
    {
        TrembleAction tremble_action = CreateInstance<TrembleAction>();
        return tremble_action;

    }

    // Start is called before the first frame update
    public override void Start()
    {
        //得到箭中靶时的位置
        arrow_pos = gameObject.transform.position;
        // this.transform.Translate(new Vector3(0, 0, 0.5f));
        Debug.Log(gameObject.transform.name);
    }

    // Update is called once per frame
    public override void Update()
    {   
        tremble_time -= Time.deltaTime;
        //需要继续颤抖
        if(tremble_time > 0){
            // gameObject.transform.Translate(new Vector3(0.02f, 0, 0));
           
            Vector3 head_pos = gameObject.transform.position;

            float x = arrow_pos.x + Random.RandomRange(-0.05f, 0.05f);
            float y = arrow_pos.y + Random.RandomRange(-0.05f, 0.05f);
            float z = arrow_pos.z;
            gameObject.transform.position = new Vector3(x, y, z);
        }
        else{
            gameObject.transform.position = arrow_pos;
        }
        // else{
        //     transform.position = arrow_pos;
        //     this.destroy = true;
        //     this.callback.SSActionEvent(this);
        // }
    }

    public override void FixedUpdate()
    {
        
    }

}
