using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCShootActionManager : SSActionManager
{
    public FirstController firstController;
    public CCShootAction shoot;

    // Start is called before the first frame update
    protected void Start()
    {
        firstController = (FirstController)SSDirector.GetInstance().CurrentScenceController;
        firstController.actionManager = this;
    }

    public void Shoot(GameObject arrow, Vector3 wind){
        Debug.Log("Shoot");
        shoot = CCShootAction.GetSSAction(wind);
        this.RunAction(arrow, shoot, this);
    }
}
